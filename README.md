# Komputer Store

#### Links: [Komputer Store](https://assignment-4-js-komputer-store.herokuapp.com/).

### Contributors
Esben Bjarnason

## Table of Contants
- [About](#about)
- [Setup](#setup)
- [Image](#image)
- [Bugs](#bugs)
- [Description](#description)
- [Tech-stack](#tech-stack)

## About
You can work for money, select/read about a laptop you want to buy, take out a loan,  
pay back a loan or transfer money you have work for into your balance account  
with a 10% deducted to any loans you may have.  
Currency=NOK 

## Setup
Use Links: [Komputer Store](https://assignment-4-js-komputer-store.herokuapp.com/).

OR

1. Clone Repository
2. Open with text editor. Like VSCode
3. VSCode: Add extension Live Server
4. VSCode: Open index.html 
5. Right click anywhere on code and click: "Open with Live Server"


## Image

![This is a alt text.](/Capture.PNG "This is a sample image.")

## Bugs  
Heroku version.  
>#### Loan Requirements square  
>* Loan can't be more than current balance * 2  
Checks for (loan **>=** balance * 2), instead of (loan **>** balance * 2)

## Description
### Bank square 
>#### Show
>* Current balance
>* Loan request Button: Let your type in a loan you want to take out if you fulfill the loan requirements
>* Outstanding Loan (Only visible after taking a loan)
>
>#### Loan Requirements square
>* Loan can't be more than current balance * 2
>* You need to own a laptop
>* You can only have one loan at the time

### Work square
>* Pay: Money earned by "Working"
>* Bank Button: Transfer 10% of your money to active loan
and the rest to your balance
>* Work Button: Increase your Pay balance by 100 each click
>* Repay Loan button:  (Only visible after taking a loan). Transfer all money from Pay balance to loan.

### Laptops square
>* Laptops toggle/menu where you can select a laptop
>* Features: Show description of selected laptop

### Laptop square
>* Name of selected laptop
>* Picture of selected laptop
>* Features: Show description of selected laptop.
>* Cost of selected pc
>* Buy Now Button: Buy selected laptop with your balance money

## Tech-stack
```
* JavaScript
* HTML
* CSS
* Heroku (Json & PHP file for Heroku)
* VSCode (VSCode extension Live Server)
```