const balanceElement = document.getElementById("balance")
const payElement = document.getElementById("pay")
const laptopNameElement = document.getElementById("laptopName")
const laptopFeaturesElement = document.getElementById("laptopFeatures")
const laptopDecElement = document.getElementById("laptopDec")
const laptopPriceElement = document.getElementById("laptopPrice")
const debtElement = document.getElementById("debt")

// button
const btnPayDebt = document.getElementById("btnPayDebt")
const btnLoanElement = document.getElementById("btnLoan")
const btnBankElement = document.getElementById("btnBank")
const btnWorkElement = document.getElementById("btnWork")
const btnBuyElement = document.getElementById("btnBuy")

// Toggle menu and image
const laptopToggleElement = document.getElementById("laptopToggle")
const laptopPicElement = document.getElementById("laptopPic")


let laptops = [];
let balance = 300.0;
let pay = 0.0;
let debt = 0.0;
// Price of display laptop
let curLaptopPrice = 0.0;
let hasLaptop = false
let hasLoan = false

// Fetch laptops API. 
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(response => response.json())
    .then(function(response) {
        laptops = response;
        laptops.forEach(element => {
            //                                  most be "option" for toggle menu
            const laptopElement = document.createElement("option")
            laptopElement.value = element.id
            laptopElement.appendChild(document.createTextNode(element.title))
                // Add all laptops to toggle menu
            laptopToggleElement.appendChild(laptopElement)
        });
        // When launching website update all UI elements
        balanceUpdate();
        payUpdate()

        // startOfProgram = set all values to laptop[0] when launching website
        laptopUpdate('startOfProgram')
    })

// Update Balance variable and UI
function balanceUpdate(addMoney = 0) {
    // Add/detract 
    balance += addMoney

    // Update UI text
    balanceElement.innerText = "Balance: " + balance + " kr";
}

// Add/Remove debt
function debtUpdate(addMoney = 0) {
    debt += addMoney

    // If you pay to much. Get change back
    let changeFromDebtPay = 0

    // Check if debt is still negative
    if (debt < 0.0) {

        debtElement.innerText = "Outstanding Loan: " + debt + " kr";
        // Display PayDebt button
        btnPayDebt.style.display = "inline"
        hasLoan = true;
    } else {
        // change for overpaying
        changeFromDebtPay = debt
        debt = 0
        debtElement.innerText = "";
        // Hide PayDebt button
        btnPayDebt.style.display = "none"
        hasLoan = false
    }
    // Returns change for overpaying. If any
    return changeFromDebtPay
}

// Get paid or get change
function payUpdate(addMoney = 0) {
    pay += addMoney
    payElement.innerText = "Pay: " + pay + " kr";
}

// Update all laptop UI
const laptopUpdate = e => {

    let selectedLaptop = []

    // If launching website set to first element in laptops array
    if (e === 'startOfProgram') {
        selectedLaptop = laptops[0];
    }
    // Get laptop from selected laptop from toggle/option element
    else {
        selectedLaptop = laptops[e.target.selectedIndex]
    }
    // Update UI
    laptopFeaturesElement.innerText = selectedLaptop.description;

    // Get image from API adr
    laptopPicElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image
    laptopNameElement.innerText = selectedLaptop.title
    laptopDecElement.innerText = selectedLaptop.description;
    curLaptopPrice = selectedLaptop.price
    laptopPriceElement.innerText = "\t" + curLaptopPrice + " kr"
}

// Check if you can get a loan.
function getALoan() {

    // User try to write in loan. totalLoan becomes input.
    const totalLoan = prompt("Please enter the amount of money you wish to loan: ");
    // Make string to float
    const loan = parseFloat(totalLoan);

    // Check if positive number
    if (isNaN(loan) || loan <= 0) {
        alert("Failed: Write in a positive number")
    }
    // Max loan = balance * 2
    else if (loan > (balance * 2)) {
        alert("Failed: Max loan " + balance * 2)
    }
    // Do you have a loan from before?
    else if (hasLoan) {
        alert("Failed: Pay back your loan first")
    }
    // Need laptop to get a loan
    else if (hasLaptop == false) {
        // to-do. One pc or one pc for every loan? You cannot get more than one bank loan before buying a computer
        alert("Failed: You need to buy a computer first")
    }
    // Success!
    else {
        debtUpdate(-loan)
        balanceUpdate(loan);
        alert(`You got loan for : ` + " " + loan + " kr");
    }
}

// Transfer money from Pay/Work to Balance
function bankTransferPayToBalance() {

    if (hasLoan) {
        // Give 10% of pay to loan
        const deducted = pay / 10
        pay -= deducted

        // Get back change if any
        pay += debtUpdate(deducted);
    }

    balanceUpdate(pay);
    payUpdate(-pay)
}

// Get paid for work
function workGetPayed() {
    payUpdate(100)
}

// Pay Debt
function payDebt() {

    pay = debtUpdate(pay)

    // If debt paid. Put rest in balance
    balanceUpdate(pay);
    pay = 0
    payUpdate()
}

// Check if you can buy a laptop
function buyLaptop() {

    // Can afford laptop
    if (balance >= curLaptopPrice) {
        balanceUpdate(-curLaptopPrice)
        hasLaptop = true
        alert("Success: You are now the owner of this new laptop")
    } else { alert("Failed: You cannot afford this laptop.") }
}

// Events
laptopToggleElement.addEventListener("change", laptopUpdate);
btnBuyElement.addEventListener("click", buyLaptop);
btnPayDebt.addEventListener("click", payDebt);
btnWorkElement.addEventListener("click", workGetPayed);
btnBankElement.addEventListener("click", bankTransferPayToBalance);
btnLoanElement.addEventListener("click", getALoan);